from django import forms

class Todo_Form(forms.Form):
    error_messages = {
        'required': 'Please fill this field',
    }
    description_attrs = {
        'type': 'text',
        'cols': 150,
        'rows': 3,
        'class': 'todo-form-textarea',
        'placeholder': 'Write Your story...'
    }

    description = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=description_attrs))
