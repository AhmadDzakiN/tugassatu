from django.test import TestCase, Client
from django.urls import resolve
from .views import index, mhs_name, birthday, gender, expertise, desc, email

# Create your tests here.
class ProfileTest(TestCase):
    
    def test_Profile_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code,200)

    def test_Profile_using_index_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, index)

    def test_expertise_is_written(self):
        #Content cannot be null
        self.assertIsNotNone(expertise)

        #Content is filled with 30 characters at least
        self.assertTrue(len(expertise) >= 5)

    def test_description_more_than_6(self):
        self.assertTrue(len(desc) >= 6)

    