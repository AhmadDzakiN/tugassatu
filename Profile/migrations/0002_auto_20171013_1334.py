# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-10-13 06:34
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Profile', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=27)),
                ('birthdate', models.DateTimeField()),
                ('gender', models.CharField(max_length=10)),
                ('email', models.EmailField(max_length=254)),
                ('descriptions', models.CharField(max_length=300)),
                ('image', models.URLField(max_length=300)),
                ('expertises', models.CharField(max_length=200)),
            ],
        ),
        migrations.DeleteModel(
            name='Message',
        ),
    ]
