from django.shortcuts import render

mhs_name = 'Aliando Syarief'
birthday = '10-10-1997'
gender = 'Male'
expertise = ['Marketing', 'Collector', 'Public Speaking','Programmer', 'Designer', 'Architect']
desc = "Antique Expert, Experience marketer for 10 years"
email = 'oktavio.rei@gmail.com' 

def index(request):
    response = {'name':mhs_name, 'birthday':birthday, 'gender':gender, 'about_me':expertise, 'description':desc, 'email':email}
    html = 'Profile/Profile.html'
    return render(request, html,  response)
