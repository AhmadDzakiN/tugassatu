from django.shortcuts import render
from update_status.models import Todo
# Create your views here.

response = {}

def index(request):
	number_of_status = Todo.objects.all().count()
	response['total'] = number_of_status
	if response['total'] != 0:
		last_post = Todo.objects.latest("created_date")
		response['last'] = last_post
	return render(request, 'Dashboard.html', response)
