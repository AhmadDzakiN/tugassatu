from django import forms

class FormLinkTeman(forms.Form):
    nama=forms.CharField(
        required=True,
        label='Nama',
        widget=forms.TextInput(attrs={'type':'text','placeholder':'Nama teman','class':'form-control'}),
        error_messages={'required':'Namanya tolong diisi'}
    )
    link=forms.URLField(
        required=True,
        label='Link',
        initial='https://',
        widget=forms.URLInput(attrs={'type':'text','placeholder':'Link heroku teman','class':'form-control'}),
        error_messages={'required':'Minta link-nya juga dong','invalid':'Link-nya isi yg bener plz (awalnya pake https://)'}
    )
