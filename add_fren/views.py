from django.shortcuts import render,redirect
from .forms import FormLinkTeman
from .models import LinkTeman

# Create your views here.

def index(request):
    response={'input_link_teman':FormLinkTeman,'link_list':LinkTeman.objects.all()}
    return render(request,'add_fren/add_fren.html',response)

def setelah(request):
    form=FormLinkTeman(request.POST or None)
    if(request.method=='POST'and form.is_valid()):
        fren=LinkTeman(nama=request.POST['nama'],link=request.POST['link'])
        fren.save()
    return redirect('/tambah-teman/')
