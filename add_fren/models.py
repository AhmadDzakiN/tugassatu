from django.db import models
from django.utils import timezone

# Create your models here.
class LinkTeman(models.Model):
    nama=models.CharField(max_length=99)
    link=models.URLField()
    tanggal_nge_add=models.DateTimeField(default=timezone.now)
