"""Penampungan URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
import add_fren.urls as add_friend
import Profile.urls as profile
import update_status.urls as update_status
import Dashboard.urls as dashboard
from django.views.generic import RedirectView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^tambah-teman/', include(add_friend,namespace='tambah-teman')),
    url(r'^profile/', include('Profile.urls')),
    url(r'^update-status/', include(update_status, namespace='update-status')),
    url(r'^stats/', include(dashboard, namespace='stats')),
    url(r'^$', RedirectView.as_view(url='/update-status/', permanent=True)),
]
